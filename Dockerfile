FROM openjdk:8-jdk-slim as build

COPY ./m2 /root/.m2/

COPY ./.mvn/ /application/.mvn

COPY ./mvnw ./pom.xml /application/

WORKDIR /application

RUN ./mvnw dependency:resolve

COPY ./src/ /application/src/

RUN ./mvwn clean package -DskipTests

FROM openjdk:8-jre-slim

RUN groupadd application && useradd application -g application -d /application -m

COPY --from=build /application/target/api-gateway-0.0.1-SNAPSHOT.jar /application/app.jar

USER application

WORKDIR /application

CMD ["java", "-jar", "app.jar"]