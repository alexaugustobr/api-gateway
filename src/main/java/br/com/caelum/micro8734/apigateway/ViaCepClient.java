package br.com.caelum.micro8734.apigateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.Map;

@FeignClient(name = "viacep", fallback = ViaCepClient.ViaCepClientFallback.class)
public interface ViaCepClient {
    @GetMapping("/ws/{cep}/json")
    Map<String, Object> findByCEP(@PathVariable String cep);

    @Component
    static class ViaCepClientFallback implements ViaCepClient {

        @Override
        public Map<String, Object> findByCEP(String cep) {
            return new HashMap<>();
        }
    }
}
