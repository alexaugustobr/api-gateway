package br.com.caelum.micro8734.apigateway;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CircuitService {
    private final RestTemplate rest;

    public CircuitService(RestTemplate rest) {
        this.rest = rest;
    }

//    @HystrixCommand(fallbackMethod = "errorFallback")
    @Retryable(backoff = @Backoff(multiplier = 2, delay = 100L))
    public String error() {
        System.out.println("Call");
        return rest.getForObject("http://apartment/circuit/error", String.class);
    }

//    public String errorFallback() {
//        return "Some Error";
//    }

    @HystrixCommand(fallbackMethod = "slowFallback")
    public String slow() {
        return rest.getForObject("http://apartment/circuit/error", String.class);
    }

    public String slowFallback() {
        return "Too slow.";
    }
}
