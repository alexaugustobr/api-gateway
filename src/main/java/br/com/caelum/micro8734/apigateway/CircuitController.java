package br.com.caelum.micro8734.apigateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitController {

    private final CircuitService service;

    public CircuitController(RestTemplate rest, CircuitService service) {
        this.service = service;
    }

    @GetMapping("error")
    String  error(){
        return service.error();
    }

    @GetMapping("slow")
    String slow() {
        return service.slow();
    }
}
