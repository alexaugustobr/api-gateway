IMAGE_NAME=micro8734/api-gateway


build/docker:
	$(info Building image$(IMAGE_NAME))

	ln -s $$HOME/.m2 ./m2

	docker image build -t $(IMAGE_NAME) .

	rm ./m2